package buoi3;
import java.util.Calendar;

public class students {
    public int student_id;
    public String student_name;
    public String student_birthday;
    public int student_namsinh;

    public int getStudent_id(){
        return student_id;
    }

    public void setStudent_id(int student_id){
        this.student_id = student_id;
    }

    public String getStudent_name(){
        return student_name;
    }

    public void setStudent_name(String student_name){
        this.student_name = student_name;
    }

    public String getStudent_birthday(){
        return student_birthday;
    }

    public void setStudent_birthday(String student_birthday){
        this.student_birthday = student_birthday;
    }

    public int getStudent_namsinh() { return student_namsinh; }

    public void setStudent_namsinh(int student_namsinh) { this.student_namsinh = student_namsinh; }

    public int getAge(){
        Calendar now = Calendar.getInstance();
        int age = now.get(Calendar.YEAR) - this.getStudent_namsinh();
        return age;
    }

    //show-Info
    public void showInfo(){
        System.out.println("Ma sinh vien: " + this.getStudent_id());
        System.out.println("Ho ten: "+ this.getStudent_name());
        System.out.println("Nam sinh: "+ this.getStudent_birthday());
        System.out.println("Tuoi: " + this.getAge());
    }
}
