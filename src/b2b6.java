import java.util.Arrays;
import java.util.Scanner;

public class b2b6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap n=");
        int n = sc.nextInt();
        int[] a = new int[n];
        int dem=0;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
            if(a[i]>0)
                dem++;
        }

        Arrays.sort(a);
        for (int i = n - dem; i < n - 1; i++)
            for (int j = i + 1; j < n; j++)
                if (a[i] < a[j]) {
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
        for (int i = n-1; i >= 0; i--)
            System.out.print(a[i]+"  ");
    }
}
