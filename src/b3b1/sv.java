package b3b1;

public class sv {
    public int student_id;
    public String student_name;
    public String student_address;
    public float student_A1;
    public float student_A3;
    public float student_NL1;

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_address() {
        return student_address;
    }

    public void setStudent_address(String student_address) {
        this.student_address = student_address;
    }

    public float getStudent_A1() {
        return student_A1;
    }

    public void setStudent_A1(float student_A1) {
        this.student_A1 = student_A1;
    }

    public float getStudent_A3() {
        return student_A3;
    }

    public void setStudent_A3(float student_A3) {
        this.student_A3 = student_A3;
    }

    public float getStudent_NL1() {
        return student_NL1;
    }

    public void setStudent_NL1(float student_NL1) {
        this.student_NL1 = student_NL1;
    }
}
