package b3b2;

public class sanpham {
    public int sp_msp;
    public String sp_ten;
    public int sp_giamua;
    public int sp_giaban;
    public int sp_soluong;

    public int getSp_msp() {
        return sp_msp;
    }

    public void setSp_msp(int sp_msp) {
        this.sp_msp = sp_msp;
    }

    public String getSp_ten() {
        return sp_ten;
    }

    public void setSp_ten(String sp_ten) {
        this.sp_ten = sp_ten;
    }

    public int getSp_giamua() {
        return sp_giamua;
    }

    public void setSp_giamua(int sp_giamua) {
        this.sp_giamua = sp_giamua;
    }

    public int getSp_giaban() {
        return sp_giaban;
    }

    public void setSp_giaban(int sp_giaban) {
        this.sp_giaban = sp_giaban;
    }

    public int getSp_soluong() {
        return sp_soluong;
    }

    public void setSp_soluong(int sp_soluong) {
        this.sp_soluong = sp_soluong;
    }
}
